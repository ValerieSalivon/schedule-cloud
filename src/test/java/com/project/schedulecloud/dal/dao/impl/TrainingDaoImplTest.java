package com.project.schedulecloud.dal.dao.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import com.project.schedulecloud.H2JpaConfig;
import com.project.schedulecloud.dal.dao.TrainingDao;
import com.project.schedulecloud.model.TrainingModel;
import java.time.Instant;
import javax.transaction.Transactional;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = H2JpaConfig.class)
public class TrainingDaoImplTest {

    @Autowired
    private TrainingDao trainingDao;

    @Test
    public void test() {
        assertNotNull(trainingDao);
    }

    @Test
    @Transactional
    public void addTraining() {

        TrainingModel model = new TrainingModel();
        model.setTrainingName("trainingName");
        model.setTrainerName("trainerName");
        model.setFrom(Instant.now());
        model.setTo(Instant.now());

        TrainingModel result = trainingDao.addTraining(model);
        assertEquals(model, result);
    }

}
