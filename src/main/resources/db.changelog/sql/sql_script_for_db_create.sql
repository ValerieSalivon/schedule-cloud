DROP SCHEMA IF EXISTS schedule_cloud CASCADE;

CREATE SCHEMA schedule_cloud;


CREATE TABLE IF NOT EXISTS "schedule_cloud".training
(
    id            BIGSERIAL PRIMARY KEY    NOT NULL,
    training_name VARCHAR                  NOT NULL,
    trainer_name  VARCHAR                  NOT NULL,
    from_time     TIMESTAMP with time zone NOT NULL,
    to_time       TIMESTAMP with time zone NOT NULL
);

CREATE TABLE IF NOT EXISTS "schedule_cloud".client
(
    id               BIGSERIAL PRIMARY KEY NOT NULL,
    name             VARCHAR               NOT NULL,
    surname          VARCHAR               NOT NULL,
    phone_number     VARCHAR(10)           NOT NULL,
    age              INTEGER,
    unused_trainings INTEGER               not null default 0
);

CREATE TABLE IF NOT EXISTS "schedule_cloud".training_client
(
    training_id BIGINT REFERENCES schedule_cloud.training (id) ON UPDATE CASCADE ON DELETE CASCADE,
    client_id   BIGINT REFERENCES schedule_cloud.client (id) ON UPDATE CASCADE ON DELETE CASCADE,
    CONSTRAINT id PRIMARY KEY (training_id, client_id)
);


