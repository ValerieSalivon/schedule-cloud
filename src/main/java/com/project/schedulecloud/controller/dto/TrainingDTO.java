package com.project.schedulecloud.controller.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.time.Instant;

@Data
public class TrainingDTO {

    @NotNull
    private String trainingName;
    @NotNull
    private String trainerName;
    @NotNull
    private Instant from;
    @NotNull
    private Instant to;
}
