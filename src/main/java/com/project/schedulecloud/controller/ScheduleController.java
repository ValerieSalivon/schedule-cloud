package com.project.schedulecloud.controller;

import com.project.schedulecloud.controller.dto.TrainingDTO;
import com.project.schedulecloud.mapper.TrainingMapper;
import com.project.schedulecloud.model.TrainingModel;
import com.project.schedulecloud.service.ScheduleManagerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;


@Slf4j
@RestController
@RequestMapping("/schedule")
public class ScheduleController {

    private ScheduleManagerService scheduleManagerService;

    @Autowired
    public ScheduleController(ScheduleManagerService scheduleManagerService) {
        this.scheduleManagerService = scheduleManagerService;
    }


    @PostMapping("/training")
    public TrainingModel addTraining(@Valid @RequestBody TrainingDTO trainingDTO){
        TrainingModel trainingModel = TrainingMapper.toModel(trainingDTO);
        trainingModel = scheduleManagerService.createTraining(trainingModel);
        return trainingModel;
    }


}
