package com.project.schedulecloud.controller;

import com.project.schedulecloud.exception.ErrorBody;
import com.project.schedulecloud.exception.ErrorCode;
import com.project.schedulecloud.exception.TrainingCreationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;


@ControllerAdvice
public class ExceptionHandlerController extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = TrainingCreationException.class)
    public ResponseEntity trainingCreationError(TrainingCreationException ex, WebRequest request){
        ErrorBody body = new ErrorBody();
        body.setErrorCode(ErrorCode.TRAINING_INTERSECTION);
        body.setMessage("Can not create training with such training details.");
        return handleExceptionInternal(ex, body, new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        ErrorBody body = new ErrorBody();
        body.setErrorCode(ErrorCode.EMPTY_REQUIRED_FIELDS);
        body.setMessage("Not all required fields are filled.");
        return handleExceptionInternal(ex, body, headers, status, request);
    }
}
