package com.project.schedulecloud.service.impl;

import com.project.schedulecloud.dal.dao.TrainingDao;
import com.project.schedulecloud.exception.TrainingCreationException;
import com.project.schedulecloud.model.TrainingModel;
import com.project.schedulecloud.service.ScheduleManagerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.List;

@Service
public class ScheduleManagerServiceImpl implements ScheduleManagerService {

    private TrainingDao trainingDao;

    @Autowired
    public ScheduleManagerServiceImpl(TrainingDao trainingDao) {
        this.trainingDao = trainingDao;
    }

    @Override
    @Transactional
    public TrainingModel createTraining(TrainingModel training) {
        if (trainingDao.findOverlappedTrainings(training.getFrom(), training.getTo(), training.getTrainerName()).isEmpty()){
            return trainingDao.addTraining(training);
        } throw new TrainingCreationException("Training for this time with such trainer already exists.");
    }

    @Override
    public TrainingModel removeTraining(Long trainingId) {
        return null;
    }

    @Override
    public List<TrainingModel> findAllTrainings(Instant from, Instant to) {
        return null;
    }
}
