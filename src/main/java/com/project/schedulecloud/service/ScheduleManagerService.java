package com.project.schedulecloud.service;

import com.project.schedulecloud.model.TrainingModel;

import java.time.Instant;
import java.util.List;


public interface ScheduleManagerService {

    TrainingModel createTraining(TrainingModel training);
    TrainingModel removeTraining(Long trainingId);
    List<TrainingModel> findAllTrainings(Instant from, Instant to);

}
