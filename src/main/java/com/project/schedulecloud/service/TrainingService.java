package com.project.schedulecloud.service;

import com.project.schedulecloud.model.TrainingModel;
import com.project.schedulecloud.model.TrainingRequestModel;

public interface TrainingService {

    TrainingModel signUp(TrainingRequestModel trainingRequestModel);
    TrainingModel cancel(TrainingRequestModel trainingRequestModel);
}
