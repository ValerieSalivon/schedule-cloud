package com.project.schedulecloud.dal.dao.impl;

import com.project.schedulecloud.dal.dao.TrainingDao;
import com.project.schedulecloud.dal.persistence.TrainingEntity;
import com.project.schedulecloud.mapper.TrainingMapper;
import com.project.schedulecloud.model.TrainingModel;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.stereotype.Repository;

@Repository
public class TrainingDaoImpl implements TrainingDao {

    @PersistenceContext
    EntityManager entityManager;

    private final String findOverlappedTrainingsQuery = "SELECT t FROM TrainingEntity t " +
            "WHERE ( t.trainerName = :trainer ) AND ( " +
            "( :toTime > t.from ) AND ( :fromTime < t.to ) )";

    @Override
    public TrainingModel addTraining(TrainingModel trainingModel) {
        TrainingEntity trainingEntity = TrainingMapper.toEntity(trainingModel);
        entityManager.persist(trainingEntity);
        return TrainingMapper.toModel(trainingEntity);
    }

    @Override
    public TrainingModel deleteTraining(Long trainingId) {
        return null;
    }

    @Override
    public List<TrainingModel> findTrainings(Instant from, Instant to) {
        return null;
    }

    @Override
    public List<TrainingModel> findOverlappedTrainings(Instant from, Instant to, String trainer) {
        final LocalDateTime fromTime = LocalDateTime.ofInstant(from, ZoneOffset.UTC);
        final LocalDateTime toTime = LocalDateTime.ofInstant(to, ZoneOffset.UTC);
        Query query = entityManager.createQuery(findOverlappedTrainingsQuery);
        query.setParameter("trainer", trainer);
        query.setParameter("toTime", toTime);
        query.setParameter("fromTime", fromTime);
        List<TrainingEntity> overlappedTrainings = query.getResultList();
        return overlappedTrainings.stream().map(TrainingMapper::toModel).collect(Collectors.toList());
    }
}
