package com.project.schedulecloud.dal.dao;

import com.project.schedulecloud.model.TrainingModel;

import java.time.Instant;
import java.util.List;

public interface TrainingDao{

    TrainingModel addTraining(TrainingModel trainingModel);
    TrainingModel deleteTraining(Long trainingId);
    List<TrainingModel> findTrainings(Instant from, Instant to);
    List<TrainingModel> findOverlappedTrainings(Instant from, Instant to, String trainer);
}
