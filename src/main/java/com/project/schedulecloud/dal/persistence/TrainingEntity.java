package com.project.schedulecloud.dal.persistence;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "training", schema = "schedule_cloud")
@Data
@NoArgsConstructor
public class TrainingEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "training_name")
    @NotNull
    private String trainingName;

    @Column(name = "trainer_name")
    @NotNull
    private String trainerName;

    @Column(name = "from_time")
    @NotNull
    private LocalDateTime from;

    @Column(name = "to_time")
    @NotNull
    private LocalDateTime to;

    @ManyToMany(mappedBy = "trainings")
    private Set<ClientEntity> clients = new HashSet<>();
}
