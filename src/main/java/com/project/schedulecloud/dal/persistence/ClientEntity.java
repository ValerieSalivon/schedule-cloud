package com.project.schedulecloud.dal.persistence;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "client", schema = "schedule_cloud")
@Data
@NoArgsConstructor
public class ClientEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    @NotNull
    private String name;

    @Column(name = "surname")
    @NotNull
    private String surname;

    @Column(name = "phone_number")
    @NotNull
    @Size(min = 10, max = 10, message = "Phone number should consists of 10 characters")
    private String phoneNumber;

    @Column(name = "age")
    private Integer age;

    @Column(name = "unused_trainings")
    @NotNull
    private Integer unusedTrainings;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "training_client",
            joinColumns = {@JoinColumn(name = "client_id")},
            inverseJoinColumns = {@JoinColumn(name = "training_id")})
    private Set<TrainingEntity> trainings = new HashSet<>();

}
