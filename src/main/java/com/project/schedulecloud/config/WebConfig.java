package com.project.schedulecloud.config;

import com.project.schedulecloud.dal.dao.TrainingDao;
import com.project.schedulecloud.dal.dao.impl.TrainingDaoImpl;
import org.modelmapper.ModelMapper;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class WebConfig {
    @Bean
    public ModelMapper modelMapper() {
        return new ModelMapper();
    }


}
