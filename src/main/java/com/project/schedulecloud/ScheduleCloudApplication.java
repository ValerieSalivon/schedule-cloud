package com.project.schedulecloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ScheduleCloudApplication {

    public static void main(String[] args) {
        SpringApplication.run(ScheduleCloudApplication.class, args);
    }

}
