package com.project.schedulecloud.mapper;

import com.project.schedulecloud.controller.dto.TrainingDTO;
import com.project.schedulecloud.dal.persistence.TrainingEntity;
import com.project.schedulecloud.model.TrainingModel;

import java.time.LocalDateTime;
import java.time.ZoneOffset;

public class TrainingMapper {

    private TrainingMapper() {
    }

    public static TrainingEntity toEntity(TrainingModel model){
        TrainingEntity entity = new TrainingEntity();
        entity.setId(model.getId());
        entity.setTrainingName(model.getTrainingName());
        entity.setTrainerName(model.getTrainerName());
        entity.setFrom(LocalDateTime.ofInstant(model.getFrom(), ZoneOffset.UTC));
        entity.setTo(LocalDateTime.ofInstant(model.getTo(), ZoneOffset.UTC));
        return entity;
    }

    public static TrainingModel toModel(TrainingEntity entity){
        TrainingModel model = new TrainingModel();
        model.setId(entity.getId());
        model.setTrainingName(entity.getTrainingName());
        model.setTrainerName(entity.getTrainerName());
        model.setFrom(entity.getFrom().toInstant(ZoneOffset.UTC));
        model.setTo(entity.getTo().toInstant(ZoneOffset.UTC));
        return model;
    }

    public static TrainingModel toModel(TrainingDTO dto){
        TrainingModel model = new TrainingModel();
        model.setTrainingName(dto.getTrainingName());
        model.setTrainerName(dto.getTrainerName());
        model.setFrom(dto.getFrom());
        model.setTo(dto.getTo());
        return model;
    }
}
