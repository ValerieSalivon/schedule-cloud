package com.project.schedulecloud.mapper.ReflectionMapperTry;

import lombok.Data;

import java.util.List;

@Data
public class SourceClass extends MySuperClass{
    private String privSub;
    public String pubSup;
    private MySuperClass superClass;
    private List<MySuperClass> classes;
}
