package com.project.schedulecloud.mapper.ReflectionMapperTry;

import lombok.SneakyThrows;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ReflectionMapperTest {

    @SneakyThrows
    public static void main(String[] args) {

        MySuperClass mySuperClass = new MySuperClass();
        mySuperClass.setPrivSuper("privSuperValue");
        mySuperClass.setPubSuper("pubSuperValue");

        MySubClass mySubClass = new MySubClass();
        mySubClass.setPrivSub("privSubValue");
        mySubClass.setPubSup("pubSubValue");


        List<MySuperClass> classes = new ArrayList<>();
        classes.add(mySuperClass);

        SourceClass source = new SourceClass();
        source.setPrivSub("privSource");
        source.setPubSup("pubSource");
        source.setPrivSuper("1");
        source.setPubSuper("2");
        source.setSuperClass(mySuperClass);
        source.setClasses(classes);

        MySubClass destination = new MySubClass();

        destination = map(source, destination);
        System.out.println(destination.toString());

    }

    public static <S, D> D map(S source, D destination) throws IllegalAccessException, NoSuchFieldException {

        List<Field> fields = getFields(source.getClass());
        for(Field field :fields) {
            Field destField = destination.getClass().getDeclaredField(field.getName());
            field.setAccessible(true);
            destField.setAccessible(true);
            if(field.getType().getSuperclass()!=Object.class){
                System.out.println(field.getGenericType());
                Field innerField = destination.getClass().getDeclaredField(field.getName());
                innerField.setAccessible(true);
                destField.set(destination, map(field.get(source), innerField.get(destination)));
            }
            destField.set(destination, field.get(source));
        }
        return destination;
    }

    private static List<Field> getFields(Class source){
        List<Field> sourceFields = new ArrayList<>();
        while (source!=null){
            sourceFields.addAll(Arrays.asList(source.getDeclaredFields()));
            source=source.getSuperclass();
        }
        return sourceFields;
    }
}

