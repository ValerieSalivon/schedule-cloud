package com.project.schedulecloud.exception;

import lombok.Data;

@Data
public class ErrorBody {

    private String message;
    private String errorCode;

    public void setErrorCode(ErrorCode errorCode) {
        this.errorCode = errorCode.name();
    }
}
