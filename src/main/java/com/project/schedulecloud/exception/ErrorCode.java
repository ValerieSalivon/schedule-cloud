package com.project.schedulecloud.exception;


public enum ErrorCode {

    TRAINING_INTERSECTION("Training with such trainer on that time intersect with another one"),
    EMPTY_REQUIRED_FIELDS("Not all required fields are filled.");

    private String message;

    ErrorCode(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
