package com.project.schedulecloud.exception;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class TrainingCreationException extends RuntimeException {

    public TrainingCreationException(String message) {
        super(message);
    }

    public TrainingCreationException(String message, Throwable cause) {
        super(message, cause);
    }

    public TrainingCreationException(Throwable cause) {
        super(cause);
    }
}
