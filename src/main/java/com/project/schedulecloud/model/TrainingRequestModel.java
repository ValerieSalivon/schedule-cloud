package com.project.schedulecloud.model;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class TrainingRequestModel {
    @NotNull
    private Long trainingId;
    @NotNull
    private Long clientId;
}
