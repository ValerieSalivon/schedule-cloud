package com.project.schedulecloud.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.time.Instant;

@Data
@NoArgsConstructor
public class TrainingModel {

    private Long id;
    @NotNull
    private String trainingName;
    @NotNull
    private String trainerName;
    @NotNull
    private Instant from;
    @NotNull
    private Instant to;


}
